package ae.datainsights.rmjfx.controller;

import ae.datainsights.rmjfx.model.Setting;
import ae.datainsights.rmjfx.model.User;
import ae.datainsights.rmjfx.service.LoginService;
import ae.datainsights.rmjfx.service.SettingService;
import javafx.fxml.FXML;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import static ae.datainsights.rmjfx.utils.CommonUtils.infoBox;
import static ae.datainsights.rmjfx.utils.CommonUtils.showAlert;

public class LoginController {

    @FXML
    private ChoiceBox<String> serverField;
    @FXML
    private TextField usernameField;
    @FXML
    private PasswordField passwordField;
    @FXML
    private TextField tenantField;
    @FXML
    private Button submitButton;

    @FXML
    private void login(ActionEvent evt) throws Exception {

        Stage owner = (Stage) submitButton.getScene().getWindow();

        if (!serverField.getValue().isEmpty() && !tenantField.getText().isEmpty()) {
            Setting setting = SettingService.readSettings();
            setting.setServer(serverField.getValue());
            setting.setTenant(tenantField.getText());
            SettingService.saveSettings(setting);
        }

        if (usernameField.getText().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, owner, "Form Error!",
"Please enter your username");
            return;
        }
        if (passwordField.getText().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, owner, "Form Error!",
                    "Please enter a password");
            return;
        }
        if (tenantField.getText().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, owner, "Form Error!",
                    "Please enter a tenant name");
            return;
        }

        User user = new User(usernameField.getText(), passwordField.getText(), tenantField.getText());
        String authToken = LoginService.makeLoginRequest(user);
        if (authToken.isEmpty()) {
            infoBox("Invalid Credentials", null, "Failed");
            return;
        }

        LoginService.saveAuthToken(authToken);
        InitController.navigateToHomePage(owner);

    }

}
