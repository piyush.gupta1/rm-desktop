package ae.datainsights.rmjfx.controller;

import ae.datainsights.rmjfx.model.ErpResult;
import ae.datainsights.rmjfx.model.Setting;
import ae.datainsights.rmjfx.service.CustomerBalanceExportService;
import ae.datainsights.rmjfx.service.FtpService;
import ae.datainsights.rmjfx.service.ImportService;
import ae.datainsights.rmjfx.service.InvoiceExportService;
import ae.datainsights.rmjfx.service.SettingService;
import ae.datainsights.rmjfx.utils.CommonUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextInputDialog;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.Optional;

import static ae.datainsights.rmjfx.utils.Constants.*;


public class HomeController {

    public void onImportCustomersClick(ActionEvent actionEvent) throws Exception {
        Long beforeImportTime = Instant.now().toEpochMilli();
        Setting setting = SettingService.readSettings();
        ErpResult erpResult = ImportService.importCsvFromERP(actionEvent, CUSTOMERS, null, false);
        if (setting.getConfirmBeforeImport()) {
            Alert alert = CommonUtils.confirmationBox(erpResult.getCsvPath() + " file will be imported. Do you want to proceed?", null, "Confirmation");
            if (alert.getResult().equals(ButtonType.YES)) {
                ImportService.importCustomerCsvToSage(CUSTOMERS, erpResult, beforeImportTime);
            }
            else {
                ImportService.notifyFailureToERP(CUSTOMERS, erpResult.getIds());
            }
        }
        else {
            ImportService.importCustomerCsvToSage(CUSTOMERS, erpResult, beforeImportTime);
        }
    }

    public void onImportInvoicesClick(ActionEvent actionEvent) throws Exception {
        Long beforeImportTime = Instant.now().toEpochMilli();
        Setting setting = SettingService.readSettings();
        String nominalCode = setting.getInvoiceNLCode();
        Boolean exportTransactionLineDetails = setting.getIncludeTransactionLineDetails();
        ErpResult erpResult = ImportService.importCsvFromERP(actionEvent, INVOICES, nominalCode, exportTransactionLineDetails);
        if (setting.getConfirmBeforeImport()) {
            Alert alert = CommonUtils.confirmationBox(erpResult.getCsvPath() + " file will be imported. Do you want to proceed?", null, "Confirmation");
            if (alert.getResult().equals(ButtonType.YES)) {
                ImportService.importInvoiceToSage(INVOICES, erpResult, beforeImportTime);
            }
            else {
                ImportService.notifyFailureToERP(INVOICES, erpResult.getIds());
            }
        }
        else {
            ImportService.importInvoiceToSage(INVOICES, erpResult, beforeImportTime);
        }
    }

    public void onImportPaymentsClick(ActionEvent actionEvent) throws Exception {
        Long beforeImportTime = Instant.now().toEpochMilli();
        Setting setting = SettingService.readSettings();
        String nominalCode = setting.getPaymentsNLCode();
        ErpResult erpResult = ImportService.importCsvFromERP(actionEvent, PAYMENTS, nominalCode, false);
        if (setting.getConfirmBeforeImport()) {
            Alert alert = CommonUtils.confirmationBox(erpResult.getCsvPath() + " file will be imported. Do you want to proceed?", null, "Confirmation");
            if (alert.getResult().equals(ButtonType.YES)) {
                ImportService.importCsvToSageUsingTransactionTool(PAYMENTS, erpResult, beforeImportTime, true);
            }
            else {
                ImportService.notifyFailureToERP(PAYMENTS, erpResult.getIds());
            }
        }
        else {
            ImportService.importCsvToSageUsingTransactionTool(PAYMENTS, erpResult, beforeImportTime, true);
        }
    }

    public void onImportCreditNotesClick(ActionEvent actionEvent) throws Exception {
        Long beforeImportTime = Instant.now().toEpochMilli();
        Setting setting = SettingService.readSettings();
        String nominalCode = setting.getCreditNoteNLCode();
        Boolean exportTransactionLineDetails = setting.getIncludeTransactionLineDetails();
        ErpResult erpResult = ImportService.importCsvFromERP(actionEvent, CREDIT_NOTES, nominalCode, exportTransactionLineDetails);
        if (setting.getConfirmBeforeImport()) {
            Alert alert = CommonUtils.confirmationBox(erpResult.getCsvPath() + " file will be imported. Do you want to proceed?", null, "Confirmation");
            if (alert.getResult().equals(ButtonType.YES)) {
                ImportService.importCsvToSageUsingTransactionTool(CREDIT_NOTES, erpResult, beforeImportTime, true);
            }
            else {
                ImportService.notifyFailureToERP(CREDIT_NOTES, erpResult.getIds());
            }
        }
        else {
            ImportService.importCsvToSageUsingTransactionTool(CREDIT_NOTES, erpResult, beforeImportTime, true);
        }
    }

    public void onImportPurchaseInvoicesClick(ActionEvent actionEvent) throws Exception {
        Long beforeImportTime = Instant.now().toEpochMilli();
        Setting setting = SettingService.readSettings();
        String nominalCode = setting.getPurchaseInvoiceNLCode();
        ErpResult erpResult = ImportService.importCsvFromERP(actionEvent, PURCHASE_INVOICES, nominalCode, false);
        if (setting.getConfirmBeforeImport()) {
            Alert alert = CommonUtils.confirmationBox(erpResult.getCsvPath() + " file will be imported. Do you want to proceed?", null, "Confirmation");
            if (alert.getResult().equals(ButtonType.YES)) {
                ImportService.importCsvToSageUsingTransactionTool(PURCHASE_INVOICES, erpResult, beforeImportTime, true);
            }
            else {
                ImportService.notifyFailureToERP(PURCHASE_INVOICES, erpResult.getIds());
            }
        }
        else {
            ImportService.importCsvToSageUsingTransactionTool(PURCHASE_INVOICES, erpResult, beforeImportTime, true);
        }
    }

    public void onImportAllClick(ActionEvent actionEvent) throws Exception {
        onImportCustomersClick(actionEvent);
        onImportInvoicesClick(actionEvent);
        onImportPaymentsClick(actionEvent);
        onImportCreditNotesClick(actionEvent);
        onImportPurchaseInvoicesClick(actionEvent);
    }

    public void onExportCustomerBalanceClick(ActionEvent actionEvent) throws Exception {
        String filePath = CustomerBalanceExportService.exportFromSage();
        CustomerBalanceExportService.reformatExportCSV(filePath);
        CustomerBalanceExportService.ExportCsvToErp(actionEvent, filePath, CUSTOMER_BALANCE);
    }

    @FXML
    private Button importInvoiceButton;

    @FXML
    public void initialize() throws IOException {
        String tenant = SettingService.readSettings().getTenant();
        if (!tenant.equals("topgun")) {
            importInvoiceButton.setVisible(false);
        }
    }

    public void onExportInvoicesClick(ActionEvent actionEvent) throws Exception {
        TextInputDialog dialog = new TextInputDialog("");
        dialog.initStyle(StageStyle.UTILITY);
        dialog.setTitle("Leave blank to export for all customers");
        dialog.setHeaderText(null);
        dialog.setContentText("AccountRef:");

        Optional<String> customerAccRef = dialog.showAndWait();
        if (customerAccRef.isPresent()) {
            File file = InvoiceExportService.exportFromSage(customerAccRef.get());
            if (file != null) {
                FtpService.putFileToPath(file.getAbsolutePath(), "ftp/uploads/" + file.getName());
            }
        }

    }

    public void onSettingsClick(ActionEvent evt) throws IOException {
        Stage owner = (Stage) ((Node) evt.getSource()).getScene().getWindow();
        InitController.navigateToSettingsPage(owner);
    }

}
