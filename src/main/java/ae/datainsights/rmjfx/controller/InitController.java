package ae.datainsights.rmjfx.controller;

import ae.datainsights.rmjfx.Main;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

import static ae.datainsights.rmjfx.utils.Constants.*;

public class InitController extends Application {

    @Override
    public void start(Stage stage) throws IOException {
        navigateToLoginPage(stage);
    }

    public static void navigateToHomePage(Stage ownerStage) throws IOException {

        FXMLLoader loader = new FXMLLoader(Main.class.getResource(HOME_VIEW));
        Scene scene = new Scene(loader.load(), 420, 450);
        scene.getStylesheets().add(Main.class.getResource("stylesheet.css").toExternalForm());
        ownerStage.setScene(scene);
        ownerStage.show();
    }

    public static void navigateToSettingsPage(Stage ownerStage) throws IOException {
        FXMLLoader loader = new FXMLLoader(Main.class.getResource(SETTINGS_VIEW));
        Scene scene = new Scene(loader.load(), 420, 420);
        scene.getStylesheets().add(Main.class.getResource("stylesheet.css").toExternalForm());
        ownerStage.setScene(scene);
        ownerStage.show();
    }

    public static void navigateToLoginPage(Stage ownerStage) throws  IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource(LOGIN_VIEW));
        Scene scene = new Scene(fxmlLoader.load(), 420, 420);
        scene.getStylesheets().add(Main.class.getResource("stylesheet.css").toExternalForm());
        ownerStage.setTitle("Route Magic");
        ownerStage.setScene(scene);
        ownerStage.show();
    }



    public static void main(String[] args) {
        launch(args);
    }

}


