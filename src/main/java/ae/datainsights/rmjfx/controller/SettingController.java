package ae.datainsights.rmjfx.controller;

import ae.datainsights.rmjfx.model.Setting;
import ae.datainsights.rmjfx.service.SettingService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

import static ae.datainsights.rmjfx.utils.CommonUtils.showAlert;

public class SettingController {

    @FXML
    private TextField PaymentsNLCode;
    @FXML
    private TextField CreditNoteNLCode;
    @FXML
    private TextField InvoiceNLCode;
    @FXML
    private TextField PurchaseInvoiceNLCode;
    @FXML
    private CheckBox ImportInvoicesAsSaleOrders;
    @FXML
    private CheckBox IncludeTransactionLineDetails;
    @FXML
    private CheckBox ConfirmBeforeImport;
    @FXML
    private CheckBox IncludeEmailInCustomerImport;
    @FXML
    private Button saveButton;

    public void initialize() throws IOException {
        Setting setting = SettingService.readSettings();
        PaymentsNLCode.setText(setting.getPaymentsNLCode());
        CreditNoteNLCode.setText(setting.getCreditNoteNLCode());
        InvoiceNLCode.setText(setting.getInvoiceNLCode());
        PurchaseInvoiceNLCode.setText(setting.getPurchaseInvoiceNLCode());
        ImportInvoicesAsSaleOrders.setSelected(setting.getImportInvoicesAsSaleOrders());
        IncludeTransactionLineDetails.setSelected(setting.getIncludeTransactionLineDetails());
        ConfirmBeforeImport.setSelected(setting.getConfirmBeforeImport());
        IncludeEmailInCustomerImport.setSelected(setting.getIncludeEmailInCustomerImport());

        if (setting.getImportInvoicesAsSaleOrders()) {
            IncludeTransactionLineDetails.setDisable(true);
        }

        ImportInvoicesAsSaleOrders.setOnAction(event -> {
            if (ImportInvoicesAsSaleOrders.isSelected()) {
                IncludeTransactionLineDetails.setDisable(true);
            } else {
                IncludeTransactionLineDetails.setDisable(false);
            }
        });
    }

    @FXML
    private void updateSettings(ActionEvent evt) throws Exception {
        Stage owner = (Stage) saveButton.getScene().getWindow();

        if (PaymentsNLCode.getText().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, owner, "Form Error!",
                    "Please enter Nominal Code for Payments");
            return;
        }
        if (CreditNoteNLCode.getText().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, owner, "Form Error!",
                    "Please enter Nominal Code for Credit Notes");
            return;
        }

        Setting setting = SettingService.readSettings();
        setting.setPaymentsNLCode(PaymentsNLCode.getText());
        setting.setCreditNoteNLCode(CreditNoteNLCode.getText());
        setting.setInvoiceNLCode(InvoiceNLCode.getText());
        setting.setPurchaseInvoiceNLCode(PurchaseInvoiceNLCode.getText());
        setting.setImportInvoicesAsSaleOrders(ImportInvoicesAsSaleOrders.isSelected());
        setting.setIncludeTransactionLineDetails(IncludeTransactionLineDetails.isSelected());
        setting.setConfirmBeforeImport(ConfirmBeforeImport.isSelected());
        setting.setIncludeEmailInCustomerImport(IncludeEmailInCustomerImport.isSelected());
        SettingService.saveSettings(setting);

        InitController.navigateToHomePage(owner);
    }
}
