package ae.datainsights.rmjfx.model;

public class User {
    String username;
    String password;
    String tenant;

    public User (String username, String password, String tenant) {
        this.username = username;
        this.password = password;
        this.tenant = tenant;
    }

    @Override
    public String toString() {
        return "{" +
                "\"username\": \"" + username + '\"' +
                ", \"password\": \"" + password + '\"' +
                ", \"tenant\": \"" + tenant + '\"' +
                "}";
    }
}

