package ae.datainsights.rmjfx.model;

public class ErpResult {
    String csvPath;
    String ids;

    public ErpResult() {
    }

    public String getCsvPath() {
        return csvPath;
    }
    public void setCsvPath(String csvPath) {
        this.csvPath = csvPath;
    }
    public String getIds() {
        return ids;
    }
    public void setIds(String ids) {
        this.ids = ids;
    }
}
