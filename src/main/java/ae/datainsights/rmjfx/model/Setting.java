package ae.datainsights.rmjfx.model;

public class Setting {
    private String paymentsNLCode;
    private String creditNoteNLCode;
    private String invoiceNLCode;
    private String purchaseInvoiceNLCode;

    private Boolean importInvoicesAsSaleOrders;
    private Boolean includeTransactionLineDetails;
    private Boolean confirmBeforeImport;
    private Boolean includeEmailInCustomerImport;

    // technically not settings
    private String server;
    private String tenant;
    private Long lastExportedInvoiceNo;



    public Setting() {}

    public String getInvoiceNLCode() {
        return invoiceNLCode;
    }

    public void setInvoiceNLCode(String invoiceNLCode) {
        this.invoiceNLCode = invoiceNLCode;
    }

    public String getPurchaseInvoiceNLCode() {
        return purchaseInvoiceNLCode;
    }

    public void setPurchaseInvoiceNLCode(String purchaseInvoiceNLCode) {
        this.purchaseInvoiceNLCode = purchaseInvoiceNLCode;
    }

    public String getPaymentsNLCode() {
        return paymentsNLCode;
    }

    public void setPaymentsNLCode(String paymentsNLCode) {
        this.paymentsNLCode = paymentsNLCode;
    }

    public String getCreditNoteNLCode() {
        return creditNoteNLCode;
    }

    public void setCreditNoteNLCode(String creditNoteNLCode) {
        this.creditNoteNLCode = creditNoteNLCode;
    }

    public Boolean getImportInvoicesAsSaleOrders() { return importInvoicesAsSaleOrders; }

    public void setImportInvoicesAsSaleOrders(Boolean importInvoicesAsSaleOrders) {
        this.importInvoicesAsSaleOrders = importInvoicesAsSaleOrders;
    }

    public Boolean getIncludeTransactionLineDetails() {
        return includeTransactionLineDetails;
    }

    public void setIncludeTransactionLineDetails(Boolean includeTransactionLineDetails) {
        this.includeTransactionLineDetails = includeTransactionLineDetails;
    }

    public Boolean getConfirmBeforeImport() {
        return confirmBeforeImport;
    }

    public void setConfirmBeforeImport(Boolean confirmBeforeImport) {
        this.confirmBeforeImport = confirmBeforeImport;
    }

    public Boolean getIncludeEmailInCustomerImport() {
        return includeEmailInCustomerImport;
    }

    public void setIncludeEmailInCustomerImport(Boolean includeEmailInCustomerImport) {
        this.includeEmailInCustomerImport = includeEmailInCustomerImport;
    }


    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public String getTenant() {
        return tenant;
    }

    public void setTenant(String tenant) {
        this.tenant = tenant;
    }

    public Long getLastExportedInvoiceNo() {
        return lastExportedInvoiceNo;
    }

    public void setLastExportedInvoiceNo(Long lastExportedInvoiceNo) {
        this.lastExportedInvoiceNo = lastExportedInvoiceNo;
    }
}
