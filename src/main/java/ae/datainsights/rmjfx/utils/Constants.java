package ae.datainsights.rmjfx.utils;

import java.util.Map;

public final class Constants {
    public static final String CUSTOMERS = "Customers";
    public static final String INVOICES = "Invoices";
    public static final String PAYMENTS = "Payments";
    public static final String CREDIT_NOTES = "CreditNotes";
    public static final String PURCHASE_INVOICES = "PurchaseInvoices";
    public static final String CUSTOMER_BALANCE = "CustomerBalance";
    public static final String PRODUCTION_URL = "https://prod-api.routemagic.co.uk/";
    public static final String STAGING_URL = "https://staging-api.routemagic.co.uk/";

    public static final String LOGIN_VIEW = "login-view.fxml";
    public static final String HOME_VIEW = "home-view.fxml";
    public static final String SETTINGS_VIEW = "setting-view.fxml";

    public static final Map<String, String> URL_MAP = Map.of(
            "Invoices", "api/invoices/export/sage",
            "Payments", "api/payments/export/sage",
            "CreditNotes", "api/credit-notes/export/sage",
            "PurchaseInvoices", "api/purchase-invoices/export/sage",
            "Customers", "api/customers/export/sage",
            "CustomerBalance", "api/import/customer-balance/csv?bySecondaryAccount=true"
    );

    public static final Map<String, String> URL_MAP_TO_NOTIFY_FAILURE = Map.of(
            "Invoices", "api/invoices/mark-unexported",
            "Payments", "api/payments/mark-unexported",
            "CreditNotes", "api/credit-notes/mark-unexported",
            "PurchaseInvoices", "api/purchase-invoices/mark-unexported",
            "Customers", "api/customers/mark-unexported"
    );

    // TOPGUN ftp details
    public static final String FTP_HOST = "ftp.routemagic.co.uk";
    public static final int FTP_PORT = 22;
    public static final String FTP_USER = "topgun";
    public static final String FTP_PASSWORD = "l(sChEdu^47j3dnm";
}
