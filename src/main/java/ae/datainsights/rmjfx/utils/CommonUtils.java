package ae.datainsights.rmjfx.utils;

import ae.datainsights.rmjfx.service.SettingService;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.Region;
import javafx.stage.Window;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import static ae.datainsights.rmjfx.utils.Constants.PRODUCTION_URL;
import static ae.datainsights.rmjfx.utils.Constants.STAGING_URL;

public class CommonUtils {

    public static void infoBox(String infoMessage, String headerText, String title) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setContentText(infoMessage);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.show();
    }

    public static Alert confirmationBox(String infoMessage, String headerText, String title) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setContentText(infoMessage);
        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.getButtonTypes().setAll(ButtonType.YES, ButtonType.NO);
        alert.showAndWait();
        return alert;
    }

    public static void showAlert(Alert.AlertType alertType, Window owner, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.initOwner(owner);
        alert.show();
    }

    public static void debugScript(InputStream is, String fileName) {
        try {
            File out = new File(fileName);
            Files.copy(is, out.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static String getBaseURL() throws IOException {
        String server = SettingService.readSettings().getServer();
        if (server.equals("production")) {
            return PRODUCTION_URL;
        } else {
            return STAGING_URL;
        }
    }
}
