package ae.datainsights.rmjfx;

import ae.datainsights.rmjfx.controller.InitController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        InitController.main(args);
    }
}