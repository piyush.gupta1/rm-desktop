package ae.datainsights.rmjfx.service;

import ae.datainsights.rmjfx.model.Setting;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class SettingService {

    public static void saveSettings(Setting setting) {
        Properties props = new Properties();

        props.setProperty("paymentsNLCode", setting.getPaymentsNLCode());
        props.setProperty("creditNoteNLCode", setting.getCreditNoteNLCode());
        props.setProperty("invoiceNLCode", setting.getInvoiceNLCode());
        props.setProperty("purchaseInvoiceNLCode", setting.getPurchaseInvoiceNLCode());
        props.setProperty("importInvoicesAsSaleOrders", setting.getImportInvoicesAsSaleOrders().toString());
        props.setProperty("exportTransactionLines", setting.getIncludeTransactionLineDetails().toString());
        props.setProperty("confirmBeforeImport", setting.getConfirmBeforeImport().toString());
        props.setProperty("includeEmailInCustomerImport", setting.getIncludeEmailInCustomerImport().toString());
        props.setProperty("server", setting.getServer());
        props.setProperty("tenant", setting.getTenant());
        props.setProperty("lastExportedInvoiceNo", setting.getLastExportedInvoiceNo().toString());

        try {
            File configFile = new File("config.xml");
            FileOutputStream out = new FileOutputStream(configFile);
            props.storeToXML(out,"Configuration");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Setting readSettings() throws IOException {
        Properties props = new Properties();
        FileInputStream in = new FileInputStream("config.xml");
        props.loadFromXML(in);

        Setting setting = new Setting();
        setting.setPaymentsNLCode(props.getProperty("paymentsNLCode", "1200"));
        setting.setCreditNoteNLCode(props.getProperty("creditNoteNLCode", "4000"));
        setting.setInvoiceNLCode(props.getProperty("invoiceNLCode", "4000"));
        setting.setPurchaseInvoiceNLCode(props.getProperty("purchaseInvoiceNLCode", "5000"));
        setting.setImportInvoicesAsSaleOrders(Boolean.valueOf(props.getProperty("importInvoicesAsSaleOrders", "0")));
        setting.setIncludeTransactionLineDetails(Boolean.valueOf(props.getProperty("exportTransactionLines", "0")));
        setting.setConfirmBeforeImport(Boolean.valueOf(props.getProperty("confirmBeforeImport", "0")));
        setting.setIncludeEmailInCustomerImport(Boolean.valueOf(props.getProperty("includeEmailInCustomerImport", "1")));
        setting.setServer(props.getProperty("server", "staging"));
        setting.setTenant(props.getProperty("tenant", ""));
        setting.setLastExportedInvoiceNo(
                Long.valueOf(props.getProperty("lastExportedInvoiceNo", "0"))
        );

        return setting;
    }
}
