package ae.datainsights.rmjfx.service;

import ae.datainsights.rmjfx.model.Setting;
import com.opencsv.CSVReader;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static ae.datainsights.rmjfx.utils.CommonUtils.infoBox;

public class InvoiceExportService {

    public static File exportFromSage(String customerAccRef) {
        try {
            Setting setting = SettingService.readSettings();
            long startingInvoiceNo = setting.getLastExportedInvoiceNo();
            File formatFile = new File("Files/templates/invoiceToSaleOrderExportFormat.csv");
            File fileToExport = new File("Files\\" + System.currentTimeMillis() + "_Invoice_As_Order.csv");
            String cmdStr = "\"C:\\Program Files (x86)\\Line50ii\\Line50ii.exe\" -F " +
                    formatFile.getAbsolutePath() + " -E " + fileToExport.getAbsolutePath() +
                    " -B " + startingInvoiceNo;
            if (Objects.nonNull(customerAccRef) && !customerAccRef.isEmpty()) {
                cmdStr += " -R " + customerAccRef;
            }
            ProcessBuilder processBuilder = new ProcessBuilder("cmd", "/c", cmdStr);
            Process p = processBuilder.start();
            int exitValue = p.onExit().get().exitValue();

            if(exitValue != 0) {
                infoBox("Unable to run Adept properly!", null, "Failed");
                throw new Exception("Export from Sage Failed");
            }

            setting = setLastExportedInvoiceNo(fileToExport, setting);
            SettingService.saveSettings(setting);

            return fileToExport;

        } catch (Exception e) {
            infoBox("Export from Sage failed", null, "Failed");
            return null;
        }
    }

    private static Setting setLastExportedInvoiceNo(File csv, Setting setting) throws IOException {
        FileReader filereader = new FileReader(csv);
        CSVReader csvReader = new CSVReader(filereader);
        String[] nextRecord = csvReader.readNext();
        List<Long> invoiceNos = new ArrayList<>();
        while ((nextRecord = csvReader.readNext()) != null) {
            invoiceNos.add(Long.valueOf(nextRecord[8]));
        }

        if (!invoiceNos.isEmpty()) {
            Long lastExportedInvoiceNo = Collections.max(invoiceNos);
            setting.setLastExportedInvoiceNo(lastExportedInvoiceNo);
        }

        return setting;
    }
}
