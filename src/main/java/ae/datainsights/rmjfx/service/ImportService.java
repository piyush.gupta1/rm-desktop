package ae.datainsights.rmjfx.service;

import ae.datainsights.rmjfx.controller.InitController;
import ae.datainsights.rmjfx.model.ErpResult;
import ae.datainsights.rmjfx.model.Setting;
import ae.datainsights.rmjfx.utils.CommonUtils;
import com.opencsv.CSVReader;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import static ae.datainsights.rmjfx.utils.CommonUtils.infoBox;
import static ae.datainsights.rmjfx.utils.Constants.*;
import static java.util.Objects.nonNull;

public class ImportService {

    public static ErpResult importCsvFromERP(ActionEvent evt, String entity, String nominalCode, Boolean exportLineDetails) throws Exception {
        try {
            String baseUrl = CommonUtils.getBaseURL();
            String token = LoginService.getAuthToken();


            if (SettingService.readSettings().getImportInvoicesAsSaleOrders()) {
                String params = "?asSaleOrder=" + "true";
                return fetchFromERP(evt, entity, baseUrl, token, params);
            }

            String params = "";
            if (nonNull(nominalCode)) {
                params = "?nominalCode=" + nominalCode;
            }
            if (exportLineDetails) {
                params = "?lineDetail=" + "true";
            }
            if (nonNull(nominalCode) && exportLineDetails) {
                params = "?nominalCode=" + nominalCode + "&lineDetail=" + "true";
            }

            return fetchFromERP(evt, entity, baseUrl, token, params);
        }
        catch (Exception e) {
            throw new Exception(e);
        }
    }

    private static ErpResult fetchFromERP(ActionEvent evt, String entity, String baseUrl, String token, String params) throws URISyntaxException, IOException, InterruptedException {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI(baseUrl + URL_MAP.get(entity) + params))
                .version(HttpClient.Version.HTTP_2)
                .GET()
                .header("Authorization", "Bearer " + token.trim())
                .build();

        String fileName = "Files\\" + System.currentTimeMillis() + "_" + entity + ".csv";
        HttpResponse<Path> response = client.send(request,
                HttpResponse.BodyHandlers.ofFile(Path.of(fileName)));

        if (response.statusCode() == 401) {
            infoBox("Session Expired", null, "Failed");
            Stage owner = (Stage) ((Node) evt.getSource()).getScene().getWindow();
            InitController.navigateToLoginPage(owner);
        }
        else if (response.statusCode() != 200) {
            infoBox("Export from ERP Failed", null, "Failed");
        }

        String csvPath = response.body().toFile().getAbsolutePath();
        boolean includeEmail = SettingService.readSettings().getIncludeEmailInCustomerImport();
        if (!includeEmail) {
            removeCustomerEmailFromImportCSV(csvPath);
        }

        ErpResult erpResult = new ErpResult();
        erpResult.setCsvPath(csvPath);
        erpResult.setIds(response.headers().firstValue("ids").orElse(""));
        return erpResult;
    }

    public static boolean importCsvToSageUsingTransactionTool(String entity, ErpResult erpResult, Long beforeImportTime, boolean showSuccessDialog) throws Exception {
        boolean success = false;
        try {
            String transactionToolFaultyLoc = "C:\\l50it\\Faulty";
            String cmdStr = "\"C:\\Program Files (x86)\\Line50it\\Line50it.exe\" -F " + erpResult.getCsvPath();
            success = runImportTool(beforeImportTime, transactionToolFaultyLoc, cmdStr);
            if (showSuccessDialog && success) {
                infoBox("Import Successful", null, "Success");
            }
        } catch (Exception e) {
            notifyFailureToERP(entity, erpResult.getIds());
            infoBox("Import to Sage Failed", null, "Failed");
        }
        return success;
    }

    private static boolean runImportTool(Long beforeImportTime, String faultyLoc, String cmdStr) throws Exception {
        boolean success = false;
        ProcessBuilder processBuilder = new ProcessBuilder("cmd", "/c", cmdStr);
        Process p = processBuilder.start();

        int exitValue = p.onExit().get().exitValue();

        if(exitValue != 0) {
            infoBox("Unable to run Adept properly!", null, "Failed");
            throw new Exception("Import to Sage Failed");
        }
        else if (checkSageImportFailure(beforeImportTime, faultyLoc)){
            throw new Exception("Import to Sage Failed");
        }
        else {
            success = true;
        }

        return success;
    }

    public static void importCsvToSageUsingSOTool(String entity, ErpResult erpResult, Long beforeImportTime) throws Exception {
        try {
            String soToolFaultyLoc = "C:\\L50IO\\Faulty";
            String cmdStr = "\"C:\\Program Files (x86)\\Line50io\\Line50io.exe\" -F " + erpResult.getCsvPath();
            boolean success = runImportTool(beforeImportTime, soToolFaultyLoc, cmdStr);
            if (success) {
                infoBox("Import Successful", null, "Success");
            }
        } catch (Exception e) {
            notifyFailureToERP(entity, erpResult.getIds());
            infoBox("Import to Sage Failed", null, "Failed");
        }
    }

    public static void importInvoiceToSage(String entity, ErpResult erpResult, Long beforeImportTime) throws Exception {
        if (SettingService.readSettings().getImportInvoicesAsSaleOrders()) {
            importCsvToSageUsingSOTool(entity, erpResult, beforeImportTime);
        }
        else {
            List<String> paths = moveNegativeInvoiceToCreditNotes(erpResult.getCsvPath());
            erpResult.setCsvPath(paths.get(0));
            boolean invoiceImportSuccess = importCsvToSageUsingTransactionTool(INVOICES, erpResult, beforeImportTime, false);
            boolean creditNoteImportSuccess = false;
            if (paths.size() > 1) {
                // negative invoices exists & moved to credit notes, lets import them too
                erpResult.setCsvPath(paths.get(1));
                creditNoteImportSuccess = importCsvToSageUsingTransactionTool(INVOICES, erpResult, beforeImportTime, false);
            }
            if (invoiceImportSuccess && creditNoteImportSuccess) {
                infoBox("Import Successful", null, "Success");
            }
        }
    }


    public static boolean checkSageImportFailure(Long beforeImportTime, String loc) {
        File directoryPath = new File(loc);
        if (!directoryPath.exists()) {
            infoBox("Directory" + loc+ " not found", null, "Directory not found");
            return false;
        }
        Optional<File> latestFile = Arrays.stream(directoryPath.listFiles(File::isFile))
                .max(Comparator.comparingLong(File::lastModified));

        return latestFile.isPresent() && latestFile.get().lastModified() > beforeImportTime;
    }

    public static void notifyFailureToERP(String entity, String entityIdsStr) throws Exception {
        String baseUrl = CommonUtils.getBaseURL();
        String token = LoginService.getAuthToken();
        String body = "[" + entityIdsStr + "]";

        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI(baseUrl + URL_MAP_TO_NOTIFY_FAILURE.get(entity)))
                .version(HttpClient.Version.HTTP_2)
                .POST(HttpRequest.BodyPublishers.ofByteArray(body.getBytes()))
                .header("Authorization", "Bearer " + token.trim())
                .build();

        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
    }

    public static void removeCustomerEmailFromImportCSV(String csvPath) throws IOException {
        FileReader filereader = new FileReader(csvPath);
        CSVReader csvReader = new CSVReader(filereader);
        List<String> input = new ArrayList<>();

        String[] nextRecord;
        while ((nextRecord = csvReader.readNext()) != null) {
            StringBuilder line = new StringBuilder();
            for (int i=0; i<nextRecord.length; i++) {
                if (i==8) {
                    continue;
                }
                line.append(nextRecord[i]).append(",");
            }
            line.deleteCharAt(line.length() - 1);
            input.add(line.toString());
        }

        try (PrintWriter out = new PrintWriter(csvPath)) {
            for (String inputLine: input) {
                out.println(inputLine);
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    static List<String> moveNegativeInvoiceToCreditNotes(String invoiceCsvPath) throws IOException {
        List<String> invoiceInput = new ArrayList<>();
        List<String> creditNoteInput = new ArrayList<>();

        try (FileReader filereader = new FileReader(invoiceCsvPath)) {
            CSVReader csvReader = new CSVReader(filereader);
            String[] nextRecord = csvReader.readNext();
            invoiceInput.add(String.join(",", nextRecord));
            creditNoteInput.add(String.join(",", nextRecord));
            while ((nextRecord = csvReader.readNext()) != null) {
                if (nextRecord[5].startsWith("-") && nextRecord[7].startsWith("-")) {
                    nextRecord[0] = "SC";
                    nextRecord[5] = nextRecord[5].replace("-", "");
                    nextRecord[7] = nextRecord[7].replace("-", "");
                    Setting setting = SettingService.readSettings();
                    if (Objects.equals(nextRecord[2], setting.getInvoiceNLCode())) {
                        nextRecord[2] = setting.getCreditNoteNLCode();
                    }
                    creditNoteInput.add(String.join(",", nextRecord));
                }
                else if (nextRecord[5].startsWith("-")) {
                    // if only subtotal is negative
                    // then create a new invoice with 0 subtotal and same tax
                    String[] invoiceRecord = nextRecord.clone();
                    invoiceRecord[5] = "0";
                    invoiceInput.add(String.join(",", invoiceRecord));

                    // and create a credit note with same subtotal and 0 tax
                    String[] creditNoteRecord = nextRecord.clone();
                    creditNoteRecord[0] = "SC";
                    creditNoteRecord[5] = nextRecord[5].replace("-", "");
                    creditNoteRecord[7] = "0";
                    Setting setting = SettingService.readSettings();
                    if (Objects.equals(nextRecord[2], setting.getInvoiceNLCode())) {
                        creditNoteRecord[2] = setting.getCreditNoteNLCode();
                    }
                    creditNoteInput.add(String.join(",", creditNoteRecord));
                }
                else if (nextRecord[7].startsWith("-")) {
                    // if only tax is negative
                    // then create a new invoice with same subtotal and 0 tax
                    String[] invoiceRecord = nextRecord.clone();
                    invoiceRecord[7] = "0";
                    invoiceInput.add(String.join(",", invoiceRecord));

                    // and create a credit note with 0 subtotal and same tax
                    String[] creditNoteRecord = nextRecord.clone();
                    creditNoteRecord[0] = "SC";
                    creditNoteRecord[5] = "0";
                    creditNoteRecord[7] = nextRecord[7].replace("-", "");
                    Setting setting = SettingService.readSettings();
                    if (Objects.equals(nextRecord[2], setting.getInvoiceNLCode())) {
                        creditNoteRecord[2] = setting.getCreditNoteNLCode();
                    }
                    creditNoteInput.add(String.join(",", creditNoteRecord));
                }
                else {
                    invoiceInput.add(String.join(",", nextRecord));
                }
            }
        }

        if (!invoiceInput.isEmpty()) {
            invoiceCsvPath = "Files\\" + System.currentTimeMillis() + "_Invoice_corrected.csv";
            Path invoicePath = Paths.get(invoiceCsvPath);
            Files.write(invoicePath, invoiceInput);
        }

        List<String> paths = new ArrayList<>(Collections.singleton(invoiceCsvPath));
        if (!creditNoteInput.isEmpty()) {
            String creditNoteCsvPath = "Files\\" + System.currentTimeMillis() + "_CreditNote_corrected.csv";
            Path creditNotePath = Paths.get(creditNoteCsvPath);
            Files.write(creditNotePath, creditNoteInput);
            paths.add(creditNoteCsvPath);
        }
        return paths;
    }

    public static void importCustomerCsvToSage(String entity, ErpResult erpResult, Long beforeImportTime) throws Exception {
        try {
            String customerToolFaultyLoc = "C:\\L50IC\\Faulty";
            String cmdStr = "\"C:\\Program Files (x86)\\Line50ic\\Line50ic.exe\" -F " + erpResult.getCsvPath();
            boolean success = runImportTool(beforeImportTime, customerToolFaultyLoc, cmdStr);
            if (success) {
                infoBox("Import Successful", null, "Success");
            }
        } catch (Exception e) {
            notifyFailureToERP(entity, erpResult.getIds());
            infoBox("Import to Sage Failed", null, "Failed");
        }
    }

}
