package ae.datainsights.rmjfx.service;

import ae.datainsights.rmjfx.model.User;
import ae.datainsights.rmjfx.utils.CommonUtils;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Files;
import java.nio.file.Path;


public class LoginService {

    public static String makeLoginRequest(User user) throws Exception {
        try {
            String baseUrl = CommonUtils.getBaseURL();
            HttpClient client = HttpClient.newHttpClient();
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(new URI(baseUrl + "api/authenticate"))
                    .version(HttpClient.Version.HTTP_2)
                    .POST(HttpRequest.BodyPublishers.ofString(user.toString()))
                    .header("Content-Type", "application/json")
                    .build();

            HttpResponse<String> response = client.send(request,
                    java.net.http.HttpResponse.BodyHandlers.ofString());

            if (response.statusCode() != 200) {
                return "";
            }

            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonNode = mapper.readTree(response.body());
            return jsonNode.get("id_token").asText();

        } catch (Exception e) {
            return "";
        }
    }

    public static void saveAuthToken(String token) {
        try (PrintWriter out = new PrintWriter("token.txt")) {
            out.println(token);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static String getAuthToken() throws IOException {
        String data = Files.readString(Path.of("token.txt"));
        return data;
    }
}




