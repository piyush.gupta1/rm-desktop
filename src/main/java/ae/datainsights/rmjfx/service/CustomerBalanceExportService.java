package ae.datainsights.rmjfx.service;

import ae.datainsights.rmjfx.controller.InitController;
import ae.datainsights.rmjfx.utils.CommonUtils;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.stage.Stage;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import com.opencsv.CSVReader;

import static ae.datainsights.rmjfx.utils.CommonUtils.infoBox;
import static ae.datainsights.rmjfx.utils.Constants.*;

public class CustomerBalanceExportService {

    public static String exportFromSage() {
        try {
            File formatFile = new File("Files/templates/customerBalanceExportFormat.csv");
            File fileToExport = new File("Files\\" + System.currentTimeMillis() + "_Customer_Balance.csv");
            String cmdStr = "\"C:\\Program Files (x86)\\Line50ic\\Line50ic.exe\" -F " +
                    formatFile.getAbsolutePath() + " -E " + fileToExport.getAbsolutePath();
            ProcessBuilder processBuilder = new ProcessBuilder("cmd", "/c", cmdStr);
            Process p = processBuilder.start();
            int exitValue = p.onExit().get().exitValue();

            if(exitValue != 0) {
                infoBox("Unable to run Adept properly!", null, "Failed");
                throw new Exception("Export from Sage Failed");
            }

            return fileToExport.getAbsolutePath();

        } catch (Exception e) {
            infoBox("Export from Sage failed", null, "Failed");
            return null;
        }
    }

    public static void reformatExportCSV(String csvPath) throws IOException {
        File csv = new File(csvPath);
        List<String> input = new ArrayList<>();
        input.add("accountNo,balance,import");

        FileReader filereader = new FileReader(csv);
        CSVReader csvReader = new CSVReader(filereader);
        String[] nextRecord = csvReader.readNext();
        while ((nextRecord = csvReader.readNext()) != null) {
            StringBuilder line = new StringBuilder();
            for (int i=0; i<nextRecord.length; i++) {
                if (i==1) {
                    continue;
                }
                line.append(nextRecord[i]).append(",");
            }
            line.append("1");
            input.add(line.toString());
        }

        try (PrintWriter out = new PrintWriter(csvPath)) {
            for (String inputLine: input) {
                out.println(inputLine);
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }

    }

    public static void ExportCsvToErp(ActionEvent actionEvent, String fileToExportPath, String entity) throws Exception {
        try {
            String baseUrl = CommonUtils.getBaseURL();
            String token = LoginService.getAuthToken();

            OkHttpClient client = new OkHttpClient().newBuilder().build();
            RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("data", fileToExportPath,
                    RequestBody.create(MediaType.parse("application/octet-stream"),
                        new File(fileToExportPath)))
                    .build();
            Request request = new Request.Builder()
                    .url(baseUrl + URL_MAP.get(entity))
                    .method("PUT", body)
                    .addHeader("Content-Type", "multipart/form-data")
                    .addHeader("Authorization", "Bearer " + token.trim())
                    .build();
            Response response = client.newCall(request).execute();

            if (response.isSuccessful()) {
                infoBox("Export Successful", null, "Success");
            }
            else if (response.code() == 401) {
                infoBox("Session Expired", null, "Failed");
                Stage owner = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
                InitController.navigateToLoginPage(owner);
            }
            else {
                infoBox("Export to ERP Failed", null, "Failed");
            }

        }
        catch (Exception e) {
            throw new Exception(e);
        }
    }

}
