package ae.datainsights.rmjfx.service;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

import java.util.Properties;

import static ae.datainsights.rmjfx.utils.CommonUtils.infoBox;
import static ae.datainsights.rmjfx.utils.Constants.*;
import static ae.datainsights.rmjfx.utils.Constants.FTP_PASSWORD;


public class FtpService {

    public static void putFileToPath(String fromPath, String toPath) throws Exception {
        Session session = null;
        Channel channel = null;
        try {
            JSch ssh = new JSch();
//            ssh.setKnownHosts("/path/of/known_hosts/file");
            session = ssh.getSession(FTP_USER, FTP_HOST, FTP_PORT);
            session.setPassword(FTP_PASSWORD);
            Properties config = new Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();
            channel = session.openChannel("sftp");
            channel.connect();
            ChannelSftp sftp = (ChannelSftp) channel;
            sftp.put(fromPath, toPath);

            infoBox("Export Successful", null, "Success");

        } catch (JSchException | SftpException e) {
            e.printStackTrace();
            infoBox("Export to FTP Failed", null, "Failed");
        } finally {
            if (channel != null) {
                channel.disconnect();
            }
            if (session != null) {
                session.disconnect();
            }
        }
    }
}
