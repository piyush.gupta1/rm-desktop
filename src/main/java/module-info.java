module ae.datainsights.rmjfx {
    requires javafx.controls;
    requires javafx.fxml;

    requires com.fasterxml.jackson.databind;
    requires java.net.http;
    requires okhttp3;
    requires opencsv;
    requires jsch;


    opens ae.datainsights.rmjfx to javafx.fxml;
    exports ae.datainsights.rmjfx;
    exports ae.datainsights.rmjfx.controller;
    exports ae.datainsights.rmjfx.model;
    opens ae.datainsights.rmjfx.controller to javafx.fxml;
    exports ae.datainsights.rmjfx.utils;
    opens ae.datainsights.rmjfx.utils to javafx.fxml;
}