# RM-Desktop


* This is a windows desktop GUI to sync invoices & with Sage
* Requirements:
    * JavaFX 17.0.2
    * Java 15.0.2
* Run:
    * `mvn clean install javafx:jlink`
    * This will generate an exe in target folder, 
    * Run that exe with jre 15.0.2 + 
